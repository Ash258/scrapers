import cliTable from 'cli-table'
import fs from 'fs'
import path from 'path'
import { CONSOLE, EXEC_PATH, COMPARE } from './ArgumentParser'

/** Default options for tables. */
let TABLE_OPTIONS = {
	chars: {
		'top': '', 'top-mid': '', 'top-left': '', 'top-right': ''
		, 'bottom': '', 'bottom-mid': '', 'bottom-left': '', 'bottom-right': ''
		, 'left': '|', 'left-mid': '', 'mid': '', 'mid-mid': ''
		, 'right': '|', 'right-mid': '', 'middle': '|',
	},
}

const TABLE_EMPTY_LINE = (number) => {
	let arr = []

	for (let i = 1; i <= number; ++i) arr.push('------')

	return arr
}

/**
 * Create aligned table header line.
 *
 * @param {Integer} number Number of columns.
 */
const TABLE_ALIGN_LINE = (number = 3) => {
	if (number < 3) console.error('Number should be bigget than 3')

	let arr = [':-----:', ':-----', ':-----:']
	for (let i = 3; i < number; ++i) arr.push(':----:')

	return arr
}

/**
 * Append options.
 *
 * @param {Object} object Options.
 *
 * @return New options.
 */
const mergeTables = (object) => {
	return Object.assign(TABLE_OPTIONS, object)
}

/**
 * Generic table.
 *
 * @return Created table object
 */
const TABLE = (string) => {
	if (!CONSOLE) TABLE_OPTIONS = mergeTables({ style: { head: [], border: [] } })

	let table

	switch (string) {
		case 'IMDB':
			TABLE_OPTIONS = mergeTables({
				head: ['Epizoda', 'Název', 'Premiéra'],
				colWidths: [9, 70, 12],
			})
			table = new cliTable(TABLE_OPTIONS)
			table.push(TABLE_ALIGN_LINE())
			break
		case 'Edna':
			if (COMPARE) {
				TABLE_OPTIONS = mergeTables({
					head: ['Epizoda', 'Název', 'Premiéra'],
					colWidths: [9, 70, 12],
				})
				table = new cliTable(TABLE_OPTIONS)
				table.push(TABLE_ALIGN_LINE())
			} else {
				TABLE_OPTIONS = mergeTables({
					head: ['Epizoda', 'Název', 'Minutáž', 'Sledovanost', 'Premiéra'],
					colWidths: [9, 70, 9, 13, 12],
				})
				table = new cliTable(TABLE_OPTIONS)
				table.push(TABLE_ALIGN_LINE(5))
			}
			break
	}

	return table
}

/**
 * Export table to file or console.
 *
 * @param {Object} table Table to be exported.
 * @param {String} fileName Filename to be written to.
 * @param {int} timeout Timeout in milliseconds.
 */
const exportTable = (table, fileName = null, timeout = 5000) => {
	// TODO: Rework with promises without timeout
	setTimeout(() => {
		let newTable = table.slice(0).sort((a, b) => {
			let epA = a[0], epB = b[0]

			if (epA < epB) return -1
			if (epA > epB) return 1
			return 0
		})

		for (let i = 0; i < newTable.length; ++i) table[i] = newTable[i]

		if (CONSOLE) {
			console.log(table.toString())
		} else {
			if (fileName) {
				const forbiddenChars = /[\\/:;*?|><"']+/g
				fileName = (fileName.charAt(0).toUpperCase() + fileName.slice(1))
					.replace(forbiddenChars, '')
					.replace(/[\s-\.]+/g, '_')
					.replace(/[&]+/g, 'and')

				const file = path.resolve(path.join(EXEC_PATH, `${fileName}.md`))
				fs.writeFile(file, table.toString(), (err) => {
					if (err) console.warn('Cannot write to file.')
					console.log('Succesfully wrote to file:', file)
				})
			}
		}
	}, timeout)
}

export { TABLE_OPTIONS, TABLE_EMPTY_LINE, exportTable, TABLE }
