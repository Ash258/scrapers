import EdnaScraper from '../Scrapers/Edna'
import WikipediaScraper from '../Scrapers/Wikipedia'
import NetflixScraper from '../Scrapers/Netflix'
import IMDBScraper from '../Scrapers/IMDB'

/**
 * Convert edna time format into minutes.
 *
 * @param {String} string Timestamtp to be formated.
 *
 * @return {Number} Length in minutes.
 */
const convertTime = (string) => {
	let arr = string.split(' '),
		number = parseInt(arr[0]),
		identifier = arr[1]

	if (arr.length === 2) {
		if (identifier.includes('hodin')) number *= 60
	} else {
		number = parseInt(arr[2]) + number * 60
	}

	return number
}

/**
 * Prefix single character values with zero.
 *
 * @param {String} string Date string with . as delimeter.
 *
 * @return Formated date string.
 */
const prefixDate = (string) => {
	let date = string.split('.')
		;[0, 1].forEach((ind) => {
			if (date[ind].length === 1) date[ind] = `0${date[ind]}`
		})

	return date.join('.')
}

/**
 * Prefix single digit number with zero
 *
 * @param {Number|String} number To be prefixed.
 */
const prefixSingleCharNumber = (number) => {
	if (number.length === 1) return `0${number}`
	return number
}

/**
 * Handle scraping and downloading episodes of given show.
 *
 * @param {Array} links Array of links to scrap.
 */
const handleDownloads = (links) => {
	links.forEach(link => {
		if (link.includes('topserialy')) {
			// TODO: Scrap topserialy
		} else if (link.includes('najserialy')) {
			// TODO: Scrap najserialy
		} else {
			// TODO: Some handler of not supported.
		}
	})
}

/**
 * Handle scraping and reporting episodes of given show.
 *
 * @param {Array} links Array of links to scrap.
 */
const handleReports = (links) => {
	links.forEach(link => {
		if (link.includes('edna')) {
			EdnaScraper.scrap(link)
		} else if (link.includes('imdb')) {
			IMDBScraper.scrap(link)
		} else if (link.includes('netflix')) {
			NetflixScraper.scrap(link)
		} else if (link.includes('wikipedia')) {
			WikipediaScraper.scrap(link)
		} else {
			console.error('Not supported URL:', link)
		}
	})
}

export { convertTime, handleDownloads, handleReports, prefixDate, prefixSingleCharNumber }
