import path from 'path'

let CONSOLE, EXEC_PATH, COMPARE

class ArgumentParser {
	constructor() {
		this.yargs = require('yargs')
			.usage('Usage: Scraper.exe [[--reports] [--downloads]] --links "edna link", "imdb link"')
			.showHelpOnFail(true)
			.option('console', {
				alias: ['c'],
				description: 'If specified, outputs will be printed to console instead of file.',
				type: 'boolean',
			})
			.option('report', {
				alias: ['r', 'reports'],
				default: true,
				description: 'If specified, urls will be used for Edna reports.',
				type: 'boolean',
			})
			.option('download', {
				alias: ['d', 'downloads'],
				default: false,
				description: 'If specified, urls will be used for parse download urls of episodes.',
				type: 'boolean',
			})
			.option('link', {
				alias: ['l', 'links'],
				description: 'Array of links.',
				type: 'array',
			})
			.option('dir', {
				alias: ['path', 'p'],
				default: '.',
				description: 'Specify where will be exported tables located.',
				type: 'string',
			})
			.option('compare', {
				alias: ['cmp'],
				default: false,
				description: 'If specified, only episode, name, date will be scraped.',
				type: 'boolean',
			})
			.option('edna', {
				description: 'Edna tv serie name with dashes. Example: according-to-jim',
				type: 'string',
			})
			.option('imdb', {
				description: 'ID of imdb title. Example: tt1213404',
				type: 'string',
			})
	}

	/**
	 * Parse given array.
	 *
	 * @param {Array} argv Array of arguments. (process.argv)
	 *
	 * @return {Object} Object with all parsed arguments.
	 */
	parse(argv) {
		this.yargs = this.yargs.parse(argv)

		if (!(this.yargs.links || this.yargs.edna || this.yargs.imdb)) {
			console.warn('Links or specific site argument is required (see Scraper.exe --help). Exiting.')
			process.exit(1)
		}

		this.yargs.report = this.yargs.download === false
		this.yargs.links = this.yargs.links ? this.yargs.links : []

		EXEC_PATH = path.resolve(this.yargs.dir)
		CONSOLE = this.yargs.console === true
		COMPARE = this.yargs.compare === true

		// Handle specific site parameters
		let url
		if (this.yargs.edna) url = `https://www.edna.cz/${this.yargs.edna}`
		if (this.yargs.imdb) url = `https://www.imdb.com/title/${this.yargs.imdb}`

		this.yargs.links.push(url)

		// Remove trailing slash
		let fixed = this.yargs.links.map(link => link = link.endsWith('/') ? link.substr(0, link.length - 1) : link)
		this.yargs.links = fixed

		return this.yargs
	}
}

export default ArgumentParser
export { CONSOLE, EXEC_PATH, COMPARE }
