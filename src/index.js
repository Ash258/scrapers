import ArgumentParser from './Helpers/ArgumentParser'
import { handleDownloads, handleReports } from './Helpers/Functions'

const ARGS = new ArgumentParser().parse(process.argv)

ARGS.download ? handleDownloads(ARGS.links) : handleReports(ARGS.links)
