import scrapeIt from 'scrape-it'
import { prefixSingleCharNumber } from '../Helpers/Functions'
import { TABLE, exportTable } from '../Helpers/Table'

const SERIE_REGEX = /S(\d+), Ep(\d+)/

/**
 * Convert IMDB episode format into custom.
 *
 * @param {String} string String to be converted with format 'SXX, EpXX'
 *
 * @return Converted episode format (SXXEXX).
 */
const convertEp = (string) => {
	let season = prefixSingleCharNumber(string.replace(SERIE_REGEX, '$1')),
		episode = prefixSingleCharNumber(string.replace(SERIE_REGEX, '$2'))

	return `S${season}E${episode}`
}

/**
 * Convert IMDB date format into custom.
 *
 * @param {String} string String to be converted.
 */
const convertDate = (string) => {
	let date = new Date(string).toLocaleDateString('cs-CZ', { year: 'numeric', month: '2-digit', day: '2-digit' }).split('/')

	return `${date[1]}.${date[0]}.${date[2]}`
}

/** Object to initial scrap seasons. */
const SEASON = {
	name: 'h3[itemprop="name"] > a',
	seasons: {
		listItem: 'select#bySeason > option',
		convert: x => x.trim(),
	},
}

/**
 * Episodes object.
 *
 * Properties:
 * 	episodes - Array of episodes with properties:
 * 		Ep - Episode identifier with format 'SXXEXX'.
 * 		Name - Episode name.
 * 		Date - Premiere date.
 */
const EPS = {
	episodes: {
		listItem: 'div.list.detail.eplist > div',
		data: {
			ep: {
				selector: 'div.image > a > div > div',
				convert: x => convertEp(x),
			},
			name: 'div.info > strong > a',
			date: {
				selector: 'div.info > div.airdate',
				convert: x => convertDate(x),
			},
		},
	},
}

class IMDBScraper {
	/**
	 * Execute scraping.
	 *
	 * @param {String} url IMDB URL to scrap. (https://www.imdb.com/title/ID)
	 */
	static scrap(link) {
		let SERIENAME = ''

		scrapeIt(`${link}/episodes`, SEASON)
			.then(({ data }) => {
				SERIENAME = data.name

				return data.seasons
			})
			.then(data => {
				let table = TABLE('IMDB')
				data.forEach((season) => {
					scrapeIt(`${link}/episodes?season=${season}`, EPS)
						.then(({ data }) => {
							data.episodes.forEach((each) => {
								table.push([each.ep, each.name, each.date])
							})
						})
				})
				exportTable(table, `${SERIENAME}_IMDB`, 15000)
			})
	}
}

export default IMDBScraper
