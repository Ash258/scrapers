import { convertTime, prefixDate } from '../Helpers/Functions'
import { COMPARE } from '../Helpers/ArgumentParser'
import { TABLE, exportTable } from '../Helpers/Table'
import scrapeIt from 'scrape-it'

/** Number of seasons. */
const SEASON = {
	seasons: {
		listItem: 'div#snippet--seasons > div > a',
		convert: x => x.replace(/Sezóna\s*(.*)/, '$1'),
	},
}

/**
 * Episodes object.
 *
 * Properties:
 * 	episodes - Array of episodes with properties:
 * 		Ep - Episode identifier with format 'SXXEXX'.
 * 		Name - Episode name.
 * 		Length - Length in minutes of episode.
 * 		WatchRate - Number of viewers.
 * 		Date - Premiere date.
 */
const EPS = {
	episodes: {
		listItem: 'ul#snippet--episodes > li:not(.adsense)',
		data: {
			ep: {
				selector: 'div.text-box > h3 > a',
				convert: x => x.replace(/(S\d+E\d+):.*/, '$1'),
			},
			name: {
				selector: 'div.text-box > h3 > a',
				convert: x => x.replace(/(S\d+E\d+):\s+(.*)/, '$2'),
			},
			length: {
				selector: 'div.labels > div > span:has(i.icon-clock)',
				convert: x => convertTime(x),
			},
			watchRate: 'div.labels > div > span:has(i.icon-chart)',
			date: {
				selector: 'div.labels > div > span:has(i.icon-date)',
				convert: x => prefixDate(x),
			},
		},
	},
}

class EdnaScraper {
	/**
	 * Execute scraping.
	 *
	 * @param {String} url Edna URL to scrap. (https://www.edna.cz/SERIENAME)
	 */
	static scrap(url) {
		let serieName = url.split('/').pop(),
			table = TABLE('Edna')

		scrapeIt(`${url}/epizody`, SEASON)
			.then(({ data }) => data)
			.then((data) => {
				data.seasons.forEach(each => {
					scrapeIt(`${url}/epizody/?season=${each}`, EPS)
						.then(({ data }) => {
							data.episodes.forEach((each) => {
								if (COMPARE) {
									table.push([each.ep, each.name, each.date])
								} else {
									table.push([each.ep, each.name, each.length, each.watchRate, each.date])
								}
							})
						})
				})
				exportTable(table, `${serieName}_Edna`)
			})
	}
}

export default EdnaScraper
