import { convertTime } from '../../src/Helpers/Functions'
import assert from 'assert'

describe('convertTime', () => {
	it('Simples', () => {
		assert.equal(convertTime('1 hodina'), 60)
		assert.equal(convertTime('22 minut'), 22)
	})
	it('Advanced', () => {
		assert.equal(convertTime('1 hodina 4 minuty'), 64)
		assert.equal(convertTime('2 hodiny'), 120)
		assert.equal(convertTime('2 hodiny 8 minut'), 128)
	})
})
