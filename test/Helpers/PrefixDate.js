import { prefixDate } from '../../src/Helpers/Functions'
import assert from 'assert'

describe('prefixDate', () => {
	it('Simples', () => {
		assert.equal(prefixDate('05.12.2008'), '05.12.2008')

		assert.equal(prefixDate('5.12.2008'), '05.12.2008')
		assert.equal(prefixDate('05.8.2008'), '05.08.2008')

		assert.equal(prefixDate('5.8.2008'), '05.08.2008')
	})
})
