var { exec } = require('pkg'),
	path = require('path'),
	root = path.resolve(__dirname + '/../binary/')

/**
 * Create options for pkg.
 *
 * @param {String} target Platform to be created.
 * @param {String} output Name of file to be created
 *
 * @return Array of pkg options.
 */
var options = (target, output) => {
	fullOutput = path.resolve(root + '/' + output)

	return ['dist/index.js', '--target', target, '--output', fullOutput]
}

exec(options('latest-win-x64', 'Scraper.exe'))
	.then(() => {
		console.log('Single windows x64 executable created.')
	})
