var crypto = require('crypto'),
	fs = require('fs'),
	path = require('path'),
	shasum = crypto.createHash('sha256'),
	Scraperexe = 'Scraper.exe',
	root = path.resolve(__dirname + '/../binary/'),
	fullexe = path.resolve(root + '/' + Scraperexe),
	hash = ''

if (!fs.existsSync(fullexe)) console.error('Cannot find file')

stream = fs.ReadStream(fullexe)
stream.on('data', (data) => {
	shasum.update(data)
})
stream.on('end', () => {
	hash = shasum.digest('hex')
	writeHash(hash)
})

var writeHash = (gen) => {
	fs.writeFile(fullexe + '.sha256', gen, (err) => {
		if (err) return console.log(err)
		console.log('Hash generated and written')
	})
}

