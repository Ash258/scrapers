<!-- markdownlint-disable MD010 -->

# Scrapers

Custom nodejs cli tool to generate edna reports.

## TODO

1. [x] Setup
	1. [x] Babel
	1. [x] Mocha
	1. [x] Pkg
		1. [x] Hash calculation
	1. [x] Deploying process
		1. [x] Tags
		1. [x] <https://github.com/inetprocess/gitlab-release>
1. [x] Scoop manifest
1. [ ] Add site specific parameters
	1. [x] Edna
		1. `--edna 'serie-name'`
	1. [x] IMDB
		1. `--imdb 'tt0106179'`
	1. [ ] Wikipedia
	1. [ ] Netflix
1. [ ] Make scraping asynchronous
	1. [ ] Remove that unnecessary timeout after scraping.
1. [ ] Fix scraping of specific sites `scrapers --cmp --edna 'trauma' --imdb 'tt1445201'`
